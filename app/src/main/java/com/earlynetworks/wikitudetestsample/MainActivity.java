package com.earlynetworks.wikitudetestsample;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissions();
    }

    public void checkPermissions(){
        TedPermission.with(this)
                .setPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                //.setRationaleMessage(R.string.permission_ratinale)
                //.setRationaleConfirmText(R.string.pemission_rationale_confirm)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                    }

                    @Override
                    public void onPermissionDenied(List<String> arrayList) {

                    }
                }).check();
    }

    public void moveToDetail(View view) {
        switch (view.getId()){
            case R.id.bt_hello: {
                Intent intent = new Intent(this, ArActivity.class);
                intent.putExtra("MODE", 1);
                startActivity(intent);
                break;
            }

            case R.id.bt_model: {
                Intent intent = new Intent(this, ArActivity.class);
                intent.putExtra("MODE", 2);
                startActivity(intent);
                break;
            }

            case R.id.bt_save_single: {
                Intent intent = new Intent(this, ArActivity.class);
                intent.putExtra("MODE", 3);
                startActivity(intent);
                break;
            }

            case R.id.bt_load_single: {
                Intent intent = new Intent(this, ArActivity.class);
                intent.putExtra("MODE", 4);
                startActivity(intent);
                break;
            }

            case R.id.bt_batch: {
                Intent intent = new Intent(this, ArActivity.class);
                intent.putExtra("MODE", 5);
                startActivity(intent);
                break;
            }
        }
    }
}