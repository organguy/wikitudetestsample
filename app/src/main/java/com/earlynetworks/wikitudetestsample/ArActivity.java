package com.earlynetworks.wikitudetestsample;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Toast;

import com.wikitude.architect.ArchitectJavaScriptInterfaceListener;
import com.wikitude.architect.ArchitectStartupConfiguration;
import com.wikitude.architect.ArchitectView;
import com.wikitude.common.camera.CameraSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ArActivity extends AppCompatActivity implements ArchitectJavaScriptInterfaceListener {

    private static final String TAG = "AR_TEST";

    protected ArchitectView architectView;

    int mode = 0;

    private File instantTargetSaveFile;
    private File savedAugmentationsFile;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
        initArView();
        getSupportActionBar().hide();
    }

    private void initData(){
        mode = getIntent().getIntExtra("MODE", 0);
        instantTargetSaveFile = new File(getExternalFilesDir(null), "SavedInstantTarget.wto");
        savedAugmentationsFile = new File(getExternalFilesDir(null), "SavedAugmentations.json");
    }

    private void initArView(){
        WebView.setWebContentsDebuggingEnabled(true);

        final ArchitectStartupConfiguration config = new ArchitectStartupConfiguration(); // Creates a config with its default values.
        config.setLicenseKey(WikitudeSDKConstants.WIKITUDE_SDK_KEY); // Has to be set, to get a trial license key visit http://www.wikitude.com/developer/licenses.
        config.setCameraPosition(CameraSettings.CameraPosition.BACK);       // The default camera is the first camera available for the system.
        config.setCameraResolution(CameraSettings.CameraResolution.AUTO);   // The default resolution is 640x480.
        config.setCameraFocusMode(CameraSettings.CameraFocusMode.CONTINUOUS);     // The default focus mode is continuous focusing.
        config.setCamera2Enabled(true);        // The camera2 api is disabled by default (old camera api is used).

        architectView = new ArchitectView(this);
        architectView.onCreate(config); // create ArchitectView with configuration

        setContentView(architectView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        architectView.addArchitectJavaScriptInterfaceListener(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        architectView.onPostCreate();

        try {

            switch (mode){
                case 1: {
                    String path = "01/index.html";
                    architectView.load(path);
                    break;
                }

                case 2: {
                    String path = "02/index.html";
                    architectView.load(path);
                    break;
                }

                case 3: {
                    String path = "03/index.html";
                    architectView.load(path);
                    break;
                }

                case 4: {
                    String path = "04/index.html";
                    architectView.load(path);
                    break;
                }

                case 5: {
                    String path = "05/index.html";
                    architectView.load(path);
                    break;
                }
            }


        } catch (IOException e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        architectView.onResume(); // Mandatory ArchitectView lifecycle call
    }

    @Override
    protected void onPause() {
        super.onPause();
        architectView.onPause(); // Mandatory ArchitectView lifecycle call
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        architectView.clearCache();
        architectView.onDestroy(); // Mandatory ArchitectView lifecycle call
    }

    @Override
    public void onJSONObjectReceived(JSONObject jsonObject) {
        try {
            switch (jsonObject.getString("action")) {
                case "save_current_instant_target":
                    saveAugmentations(jsonObject.getString("augmentations"));
                    saveCurrentInstantTarget();
                    break;
                case "load_existing_instant_target":
                    loadExistingInstantTarget();
                    break;
            }
        } catch (JSONException e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ArActivity.this, R.string.error_parsing_json, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void saveAugmentations(String data) {

        Log.d(TAG, data);

        try (FileOutputStream stream = new FileOutputStream(savedAugmentationsFile)) {
            stream.write(data.getBytes());
        } catch (IOException e) {
            Log.e(TAG, "Could not save augmentations.", e);
        }
    }

    private void saveCurrentInstantTarget() {
        architectView.callJavascript(String.format("World.saveCurrentInstantTargetToUrl(\"%s\")", instantTargetSaveFile.getAbsolutePath()));
    }

    private void loadExistingInstantTarget() {

        String filePath = instantTargetSaveFile.getAbsolutePath();
        String augmentations = loadAugmentations();

        Log.d(TAG, augmentations);

        String javascript = String.format("World.loadExistingInstantTargetFromUrl(\"%s\", %s)", filePath, augmentations);
        architectView.callJavascript(javascript);
    }

    private String loadAugmentations() {
        int length = (int) savedAugmentationsFile.length();

        byte[] bytes = new byte[length];
        String jsonString;

        try (FileInputStream in = new FileInputStream(savedAugmentationsFile)) {
            in.read(bytes);
            jsonString = new String(bytes);
        } catch (IOException e) {
            jsonString = "";
        }

        Log.d(TAG, jsonString);

        return jsonString;
    }
}