var World = {

    init: function initFn() {
        this.createOverlays();
    },

    createOverlays: function createOverlaysFn() {
       this.targetCollectionResource = new AR.TargetCollectionResource("assets/magazine.wtc", {
       	    onError: World.onError,
       	    //onLoaded: World.onLoaded("target collection loaded!!!")
       });

       this.tracker = new AR.ImageTracker(this.targetCollectionResource, {
            onError: World.onError,
            //onTargetsLoaded: World.onLoaded("onTargetsLoaded!!!")
       });

       var imgOne = new AR.ImageResource("assets/imageOne.png", {
       	    onError: World.onError,
       	    //onLoaded: World.onLoaded("ImageResource loaded!!!")
       });

       var overlayOne = new AR.ImageDrawable(imgOne, 5, {
       	translate: {
       		x: 3,
       		y: 1
       	}
       });

       var overlayTwo = new AR.ImageDrawable(imgOne, 5, {
            translate: {
                x: 0,
                y: 0,
                z: 1
            }
          });

        /*var positionable = new AR.Positionable("myPositionable", {
            snapToScreen.enabled: true,
            drawables : {
                cam : overlayOne
            }
        });*/

       this.pageOne = new AR.ImageTrackable(this.tracker, "pageOne", {
        enableExtendedTracking: true,
       	drawables: {
       		cam: overlayOne
       	},
       	onImageRecognized: World.onLoaded("onImageRecognized!!!"),
       	onError: World.onError
       });

       this.pageOne.drawables.addCamDrawable(overlayTwo);
    },

    onLoaded: function onLoaded(message) {
        alert(message);
    },

    onError: function onErrorFn(error) {
        alert(error);
    },

    hideInfoBar: function hideInfoBarFn() {
        document.getElementById("infoBox").style.display = "none";
    },

    showInfoBar: function worldLoadedFn() {
        document.getElementById("infoBox").style.display = "table";
        document.getElementById("loadingMessage").style.display = "none";
    }
};

World.init();